#!/bin/bash


path_to_file=$CUST_FILE
if [ -z $path_to_file ]; then
    #variable is not set, checking passed argument
    path_to_file=$1
    if [ -z $path_to_file ]; then
        echo "Path to file argument is not set" >&2
        exit 1
    fi
fi

if ! [ -f "$path_to_file" ]; then
    echo "$path_to_file doesn't exist" >&2
    exit 1
fi

while read line; do
    ip=$(echo $line | grep -Pom 1 '[0-9.]{7,15}')
    line=$(echo $line | sed "s/$ip//g")
    cmd=$(echo $line | grep -Pom 1 -m 1 '[a-zA-Z0-9/_]{1,50}' | head -1)
    line=$(echo $line | sed "s#$cmd##g")
    args=$(echo "$line" | sed "s/^[[:space:]]*//g")
    echo "Starting $cmd with args '$args' on node $ip"
done < $path_to_file
